#include <string.h>
#include <stdio.h>
#include "ft_concat_params.c"

int main(void)
{
	char *str1_1 = "DontPrintProgramName";
	char *str1_2 = "FirstParam";
	char *str1_3 = "SecondParam";
	char *str1_4 = "LastParam";
	char *s_arr1[] = {str1_1, str1_2, str1_3, str1_4};
	char *str1 = ft_concat_params(4, s_arr1);
	char *exp1 = "FirstParam\nSecondParam\nLastParam";
	int result1 = strcmp(str1, exp1);
	printf("%s... Got:\n%s", result1 == 0 ? "Success" : "Failure", str1);

	char *str2_1 = "DontPrintProgramName";
	char *s_arr2[] = {str2_1};
	char *str2 = ft_concat_params(1, s_arr2);
	char *exp2 = "";
	int result2 = strcmp(str2, exp2);
	printf("\n\n%s... Got:\n%s", result2 == 0 ? "Success" : "Failure", str2);

	char *str3_1 = "DontPrintProgramName";
	char *str3_2 = "OneParam";
	char *s_arr3[] = {str3_1, str3_2};
	char *str3 = ft_concat_params(2, s_arr3);
	char *exp3 = "OneParam";
	int result3 = strcmp(str3, exp3);
	printf("\n\n%s... Got:\n%s\n", result3 == 0 ? "Success" : "Failure", str3);
}
