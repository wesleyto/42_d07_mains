#include <stdio.h>
#include <stdlib.h>
#include "ft_split_whitespaces.c"

int main(void)
{
	char str0[] = "Hello This Sentence Has 6 Words";
	char str1[] = "\tAll\nThe Words Should Be Aligned To The Left\t\t\t\t\n";
	char str2[] = "\t\n\n\n\n\n\n\t\tAwesome\n\t\n\t";
	char str3[] = "\t\n \n\n\n\t    \t\n";
	char str4[] = "";
	int sizes[] = {7, 10, 2, 1, 1};

	char *cases[] = {str0, str1, str2, str3, str4};
	for (int i = 0; i < (int)sizeof(cases) / (int)sizeof(char *); i++)
	{
		char **split = ft_split_whitespaces(cases[i]);
		for (int j = 0; j < sizes[i]; j++)
		{
			printf("%d %s\n", j, split[j]);
		}
		printf("%s", "\n");
		for (int j = 0; j < sizes[i]; j++)
		{
			free(split[j]);
		}
		if (sizes[i] != 0)
			free(split);
	}
	return (0);
}