#include <stdio.h>
#include <stdlib.h>
#include "ft_range.c"

int arrcmp(int *arr1, int*arr2, int size)
{
	if (arr1 == NULL && arr2 == NULL)
	{
		return (0);
	}
	else if (arr1 == NULL || arr2 == NULL)
	{
		return (-1);
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			if (arr1[i] != arr2[i])
			{
				return (arr1[i] - arr2[i]);
			}
		}
	}
	return (0);
}

void print_arr(int *arr, int size)
{
	if (arr != NULL)
	{
		printf("[%d, ", arr[0]);
		for (int j = 1; j < size - 1; j++)
		{
			printf("%d, ", arr[j]);
		}
		printf("%d]", arr[size-1]);
	}
	else
	{
		printf("%-10s", "NULL");
	}
}

int main(void)
{
	int mins[] = {0, 4, -2, 4, 4, -10, -5};
	int maxs[] = {5, 0, 3, 4, -1, -10, -2};
	int exp0[] = {0, 1, 2, 3, 4};
	int *exp1 = NULL;
	int exp2[] = {-2, -1, 0, 1, 2};
	int *exp3 = NULL;
	int *exp4 = NULL;
	int *exp5 = NULL;
	int exp6[] = {-5, -4, -3};
	int *exps[] = {exp0, exp1, exp2, exp3, exp4, exp5, exp6};
	for (int i = 0; i < 7; i++)
	{
		int size = maxs[i] - mins[i];
		size = size > 0 ? size : 0;
		int *range = ft_range(mins[i], maxs[i]);
		int eql = arrcmp(exps[i], range, size);
		printf("Case: %-3d, %-3d || %s || Exp: ", mins[i], maxs[i], eql == 0 ? "Success" : "Failure");
		print_arr(exps[i], size);
		printf(" || %s: ", "Got");
		print_arr(range, size);
		printf("%s", "\n");
		if (range != NULL)
			free(range);
	}
}