#include <stdio.h>
#include <stdlib.h>
#include "ft_ultimate_range.c"

int arrcmp(int *arr1, int *arr2, int size)
{
	if (arr1 == NULL && arr2 == NULL)
	{
		return (0);
	}
	else if (arr1 == NULL || arr2 == NULL)
	{
		return (-1);
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			if (arr1[i] != arr2[i])
			{
				return (arr1[i] - arr2[i]);
			}
		}
	}
	return (0);
}

void print_arr(int *arr, int size)
{
	if (arr != NULL)
	{
		if (size != 1)
			printf("[%d, ", arr[0]);
		else
			printf("[%d", arr[0]);
		for (int j = 1; j < size - 1; j++)
		{
			printf("%d, ", arr[j]);
		}
		if (size != 1)
			printf("%d]", arr[size-1]);
		else
			printf("%s", "]");
	}
	else
	{
		printf("%-10s", "NULL");
	}
}

int main(void)
{
	int mins[] = {5, 0, 4, -2, 4, 4, -10, -5, 5};
	int maxs[] = {6, 5, 0, 3, 4, -1, -10, -2, 8};
	int exp0[] = {5};
	int exp1[] = {0, 1, 2, 3, 4};
	int *exp2 = NULL;
	int exp3[] = {-2, -1, 0, 1, 2};
	int *exp4 = NULL;
	int *exp5 = NULL;
	int *exp6 = NULL;
	int exp7[] = {-5, -4, -3};
	int exp8[] = {5, 6, 7};
	int *exps[] = {exp0, exp1, exp2, exp3, exp4, exp5, exp6, exp7, exp8};
	for (int i = 0; i < 9; i++)
	{

		int *range = NULL;
		int size = maxs[i] - mins[i];
		size = size > 0 ? size : 0;
		ft_ultimate_range(&range, mins[i], maxs[i]);
		int eql = arrcmp(exps[i], range, size);

		printf("Case: %-3d, %-3d || %s || Exp: ", mins[i], maxs[i], eql == 0 ? "Success" : "Failure");
		print_arr(exps[i], size);
		printf(" || %s: ", "Got");
		print_arr(range, size);
		printf("%s", "\n");

		if (range != NULL)
			free(range);
	}
}
