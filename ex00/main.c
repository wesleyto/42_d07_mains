#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ft_strdup.c"

int main(void)
{
	char str0[] = "Hello There";
	char str1[] = "H";
	char str2[] = "";
	char str3[] = " <-Spaces-> ";
	char str4[] = " HWithSpace";
	char *strs[] = {str0, str1, str2, str3, str4};

	for (int i = 0; i < 5; i++)
	{
		char *new_str = ft_strdup(strs[i]);
		printf("Case: %-14s || %s || Got: %-14s\n", strs[i], (strcmp(strs[i], new_str)) == 0 ? "Success!" : "Failure!", new_str);
		free(new_str);
	}

	return (0);
}